<?php

namespace App\DataFixtures;

use App\Entity\Article;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;


class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $article1 = new Article();
        $article1->setTitle('Titre de l\'article 1')
                ->setContent('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur ullamcorper tincidunt est. Etiam at convallis orci. Aliquam erat volutpat. Nulla aliquet imperdiet porta. Donec bibendum dignissim urna, eget auctor dui mollis in. Duis gravida lacinia ante, id pulvinar nulla maximus vel. Sed commodo ultricies feugiat. In hac habitasse platea dictumst. Suspendisse quis risus nunc. Morbi odio arcu, faucibus in facilisis sed, imperdiet vitae ipsum. Duis fringilla, ex sit amet tempus hendrerit, metus felis vulputate neque, sed facilisis leo ligula dapibus augue. Praesent pulvinar id sapien sit amet condimentum. Morbi nisl lorem, aliquet sit amet nunc a, elementum faucibus libero. Proin sagittis lobortis nulla, vel consectetur tortor egestas a. Vestibulum blandit varius ex, id cursus elit pellentesque vel. Maecenas molestie nisi a lectus convallis posuere.')
                ->setDateCreation(new \DateTime('2021-02-11 13:01:00'))
                ->setPicture('https://i.pinimg.com/originals/15/66/06/156606cb031f1fe655914bf07267c810.jpg');
        $manager->persist($article1);

        $article2 = new Article();
        $article2->setTitle('Article 2')
                ->setContent('Quisque faucibus diam eget lacus pharetra, ut dapibus tellus tempor. Morbi justo augue, placerat at diam quis, efficitur hendrerit justo. Phasellus eleifend consequat libero non faucibus. Aenean condimentum erat ac mattis euismod. Suspendisse non efficitur eros. Duis rutrum finibus scelerisque. Vestibulum orci mi, varius et tempor eget, vulputate id ex. Praesent vitae urna vel eros pellentesque varius. Integer ante ligula, cursus a maximus et, imperdiet ac mi. Praesent luctus pretium nibh vel aliquam. Suspendisse bibendum dictum erat. Vestibulum vel ornare est. In hac habitasse platea dictumst. Aliquam pharetra mollis orci, ac posuere risus auctor eu. Nulla ultricies risus pulvinar rhoncus tristique.')
                ->setDateCreation(new \DateTime('2020-12-26 08:15:00'))
                ->setPicture('https://www.letelegramme.fr/images/2020/11/06/frah-au-centre-on-s-est-pris-un-coup-dans-les-jambes_5387598_676x343p.jpg?v=1');
        $manager->persist($article2);

        $manager->flush();
    }
}
